import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {KginputComponent} from './kginput/kginput.component'

const routes: Routes = [
  { path: '', redirectTo: 'home', pathMatch: 'full'},
  { path: 'home', component: KginputComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
