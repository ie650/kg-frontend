import { ComponentFixture, TestBed } from '@angular/core/testing';

import { KginputComponent } from './kginput.component';

describe('KginputComponent', () => {
  let component: KginputComponent;
  let fixture: ComponentFixture<KginputComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ KginputComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(KginputComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
