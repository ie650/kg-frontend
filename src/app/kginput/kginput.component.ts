import { Component, OnInit } from '@angular/core';
import { BackendCommunicationService } from '../backend-communication.service';
import { Entity } from './entity.model';
import { Relation } from './relation.model';
import { Dictionary } from './dict.interface';
@Component({
  selector: 'app-kginput',
  templateUrl: './kginput.component.html',
  styleUrls: ['./kginput.component.scss']
})

export class KginputComponent {
  public uniqueEntities:Map<string, Entity>  = new Map<string, Entity>();
  public text: string | undefined;
  public entities: Entity[] = [];
  public relations: Relation[] = [];
  public loading: boolean = false;
  constructor(private bcs: BackendCommunicationService) { }

  ngOnInit(): void {
    this.bcs.extractEntitiesFromText("test")
  }
  setText(text: string) {
    try {
      this.text = text;
    } catch (e) {
      console.info('could not set textarea-value');
    }
  }
  updateUniqueEntities(entities: Entity[]){
    this.uniqueEntities = new Map<string, Entity>();
   
    for (let ent of entities) {
      let e = this.uniqueEntities.get(ent.surfaceForm)
      if(e === undefined){
        ent.count = 1;
        this.uniqueEntities.set(ent.surfaceForm, ent) ;
      }
      else {
        e.count += 1;
        this.uniqueEntities.set(ent.surfaceForm, e) ;
      }
    }
    console.log(this.uniqueEntities)
  }
  textDefinded(){
    return this.text != undefined
  }
  extractText() {

    if (this.text != undefined) {
      this.loading = true;
      this.bcs.extractEntitiesFromText(this.text).subscribe((event: any) => {
        console.log(event)
        if (typeof (event) === 'object') {
          this.loading = false
          this.entities = event.entities
          this.relations = event.relations
          this.updateUniqueEntities(event.entities);

        }
      });
    }

  }

}
