import { Entity } from './entity.model';
export interface Dictionary<T> {
    [Key: string]: T;

}
