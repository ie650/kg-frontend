export class Entity {
    constructor(
        public URI: string = "",
        public offset: number = 0,
        public percentageOfSecondRank: number | undefined,
        public similarityScore: number | undefined,
        public support: number | undefined,
        public surfaceForm: string = "",
        public count:number =1

    ) {
        this.URI = URI,
            this.offset = offset,
            this.percentageOfSecondRank = percentageOfSecondRank,
            this.similarityScore = similarityScore,
            this.support = support,
            this.surfaceForm = surfaceForm
            this.count=count
    }
}