import { Injectable } from '@angular/core';
import { HttpClient, HttpEvent, HttpErrorResponse, HttpEventType, HttpHeaders } from  '@angular/common/http';
import { catchError, retry } from 'rxjs/operators';
import { throwError,map } from 'rxjs';


@Injectable({
  providedIn: 'root'
})
export class BackendCommunicationService {
  backendUrl = "http://localhost:8000"
  constructor(private http: HttpClient) { }

    public extractEntitiesFromText(text:string) {
      console.log("do here")
      let options = {
        headers: new HttpHeaders({
          'Content-Type':  'application/json'})
      }
      let body = {text:text}
      return this.http.post("http://localhost:8000/extract",body,options=options)
    }




  
}
